package de.pstorch.cube;

import org.junit.Test;

import static org.junit.Assert.*;


public class CubeTest {

    @Test
    public void orientationAndColor() {
        //					front, back, top, bottom, left, right
        Cube cube = new Cube(Color.GREEN, Color.RED, Color.BLUE, Color.YELLOW, Color.RED, Color.RED);

        assertSame(Color.GREEN, cube.getColor(Side.FRONT));
        assertSame(Color.RED, cube.getColor(Side.BACK));
        assertSame(Color.BLUE, cube.getColor(Side.TOP));
        assertSame(Color.YELLOW, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(1, cube.getOrientation().ordinal());
        assertSame(Color.YELLOW, cube.getColor(Side.FRONT));
        assertSame(Color.BLUE, cube.getColor(Side.BACK));
        assertSame(Color.GREEN, cube.getColor(Side.TOP));
        assertSame(Color.RED, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(2, cube.getOrientation().ordinal());
        assertSame(Color.RED, cube.getColor(Side.FRONT));
        assertSame(Color.GREEN, cube.getColor(Side.BACK));
        assertSame(Color.YELLOW, cube.getColor(Side.TOP));
        assertSame(Color.BLUE, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(3, cube.getOrientation().ordinal());
        assertSame(Color.BLUE, cube.getColor(Side.FRONT));
        assertSame(Color.YELLOW, cube.getColor(Side.BACK));
        assertSame(Color.RED, cube.getColor(Side.TOP));
        assertSame(Color.GREEN, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(4, cube.getOrientation().ordinal());
        assertSame(Color.GREEN, cube.getColor(Side.FRONT));
        assertSame(Color.RED, cube.getColor(Side.BACK));
        assertSame(Color.YELLOW, cube.getColor(Side.TOP));
        assertSame(Color.BLUE, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(5, cube.getOrientation().ordinal());
        assertSame(Color.BLUE, cube.getColor(Side.FRONT));
        assertSame(Color.YELLOW, cube.getColor(Side.BACK));
        assertSame(Color.GREEN, cube.getColor(Side.TOP));
        assertSame(Color.RED, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(6, cube.getOrientation().ordinal());
        assertSame(Color.RED, cube.getColor(Side.FRONT));
        assertSame(Color.GREEN, cube.getColor(Side.BACK));
        assertSame(Color.BLUE, cube.getColor(Side.TOP));
        assertSame(Color.YELLOW, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(7, cube.getOrientation().ordinal());
        assertSame(Color.YELLOW, cube.getColor(Side.FRONT));
        assertSame(Color.BLUE, cube.getColor(Side.BACK));
        assertSame(Color.RED, cube.getColor(Side.TOP));
        assertSame(Color.GREEN, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(8, cube.getOrientation().ordinal());
        assertSame(Color.RED, cube.getColor(Side.FRONT));
        assertSame(Color.RED, cube.getColor(Side.BACK));
        assertSame(Color.BLUE, cube.getColor(Side.TOP));
        assertSame(Color.YELLOW, cube.getColor(Side.BOTTOM));
        assertSame(Color.GREEN, cube.getColor(Side.LEFT));
        assertSame(Color.RED, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(9, cube.getOrientation().ordinal());
        assertSame(Color.RED, cube.getColor(Side.FRONT));
        assertSame(Color.RED, cube.getColor(Side.BACK));
        assertSame(Color.YELLOW, cube.getColor(Side.TOP));
        assertSame(Color.BLUE, cube.getColor(Side.BOTTOM));
        assertSame(Color.RED, cube.getColor(Side.LEFT));
        assertSame(Color.GREEN, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(10, cube.getOrientation().ordinal());
        assertSame(Color.RED, cube.getColor(Side.FRONT));
        assertSame(Color.RED, cube.getColor(Side.BACK));
        assertSame(Color.GREEN, cube.getColor(Side.TOP));
        assertSame(Color.RED, cube.getColor(Side.BOTTOM));
        assertSame(Color.YELLOW, cube.getColor(Side.LEFT));
        assertSame(Color.BLUE, cube.getColor(Side.RIGHT));

        assertTrue(cube.setNextOrientation());
        assertEquals(11, cube.getOrientation().ordinal());
        assertSame(Color.RED, cube.getColor(Side.FRONT));
        assertSame(Color.RED, cube.getColor(Side.BACK));
        assertSame(Color.RED, cube.getColor(Side.TOP));
        assertSame(Color.GREEN, cube.getColor(Side.BOTTOM));
        assertSame(Color.BLUE, cube.getColor(Side.LEFT));
        assertSame(Color.YELLOW, cube.getColor(Side.RIGHT));
    }

}
