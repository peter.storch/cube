package de.pstorch.cube;


public enum Orientation {

    // FRONT_RIGHT(front, right, top)
    FRONT_RIGHT(Side.FRONT, Side.RIGHT, Side.TOP),
    TOP_RIGHT(Side.BOTTOM, Side.RIGHT, Side.FRONT),
    BACK_RIGHT(Side.BACK, Side.RIGHT, Side.BOTTOM),
    BOTTOM_RIGHT(Side.TOP, Side.RIGHT, Side.BACK),
    FRONT_LEFT(Side.FRONT, Side.LEFT, Side.BOTTOM),
    TOP_LEFT(Side.TOP, Side.LEFT, Side.FRONT),
    BACK_LEFT(Side.BACK, Side.LEFT, Side.TOP),
    BOTTOM_LEFT(Side.BOTTOM, Side.LEFT, Side.BACK),
    LEFT_FRONT(Side.RIGHT, Side.BACK, Side.TOP),
    RIGHT_FRONT(Side.RIGHT, Side.FRONT, Side.BOTTOM),
    TOP_FRONT(Side.RIGHT, Side.TOP, Side.FRONT),
    BOTTOM_FRONT(Side.RIGHT, Side.BOTTOM, Side.BACK),
    LEFT_BACK(Side.LEFT, Side.BACK, Side.BOTTOM),
    RIGHT_BACK(Side.LEFT, Side.FRONT, Side.TOP),
    TOP_BACK(Side.LEFT, Side.BOTTOM, Side.FRONT),
    BOTTOM_BACK(Side.LEFT, Side.TOP, Side.BACK),
    LEFT_TOP(Side.RIGHT, Side.BACK, Side.RIGHT),
    RIGHT_TOP(Side.TOP, Side.FRONT, Side.RIGHT),
    FRONT_TOP(Side.FRONT, Side.BOTTOM, Side.RIGHT),
    BACK_TOP(Side.BACK, Side.TOP, Side.RIGHT),
    LEFT_BOTTOM(Side.TOP, Side.BOTTOM, Side.BACK),
    RIGHT_BOTTOM(Side.BOTTOM, Side.FRONT, Side.LEFT),
    FRONT_BOTTOM(Side.FRONT, Side.TOP, Side.LEFT),
    BACK_BOTTOM(Side.BACK, Side.BOTTOM, Side.LEFT);

    public Side front;

    public Side right;

    public Side top;

    Orientation(Side front, Side right, Side top) {
        this.front = front;
        this.right = right;
        this.top = top;
    }

    public static Orientation first() {
        return values()[0];
    }

    public Side getFront() {
        return front;
    }

    public Side getRight() {
        return right;
    }

    public Side getTop() {
        return top;
    }

    @Override
    public String toString() {
        return "Front: " + front + ", Right: " + right + ", Top: " + top;
    }

    public boolean hasNext() {
        return ordinal() < (values().length - 1);
    }

    public Orientation next() {
        return values()[ordinal() + 1];
    }

}
