package de.pstorch.cube;

import java.util.HashSet;
import java.util.Set;

public class CubeSolver {

    private Cube[] cubes;

    public CubeSolver(Cube... cubes) {
        this.cubes = cubes;
    }

    public static void main(String[] args) {
        //					front, back, top, bottom, left, right
        Cube cube1 = new Cube(Color.GREEN, Color.RED, Color.BLUE, Color.YELLOW, Color.RED, Color.RED);
        Cube cube2 = new Cube(Color.GREEN, Color.RED, Color.YELLOW, Color.BLUE, Color.GREEN, Color.YELLOW);
        Cube cube3 = new Cube(Color.RED, Color.BLUE, Color.GREEN, Color.GREEN, Color.BLUE, Color.YELLOW);
        Cube cube4 = new Cube(Color.BLUE, Color.RED, Color.YELLOW, Color.RED, Color.YELLOW, Color.GREEN);

        CubeSolver cubeSolver = new CubeSolver(cube1, cube2, cube3, cube4);
        System.out.println(cubeSolver.solve());
    }

    public boolean solve() {
        int iteration = 0;
        boolean solved = false;
        while (!solved && moreOrientationsAvailable()) {
            iteration++;
            for (int i = 0; i < cubes.length; i++) {
                if (cubes[i].setNextOrientation()) {
                    for (int y = 0; y < i; y++) {
                        cubes[y].resetOrientation();
                    }
                    break;
                }
            }
            solved = solved();
        }
        System.out.println(iteration);
        return solved;
    }

    private boolean moreOrientationsAvailable() {
        for (Cube cube : cubes) {
            if (cube.hasMoreOrientations()) {
                return true;
            }
        }
        return false;
    }

    private boolean solved() {
        if (!allDifferent(Side.FRONT)) {
            return false;
        }
        if (!allDifferent(Side.BACK)) {
            return false;
        }
        if (!allDifferent(Side.TOP)) {
            return false;
        }
        if (!allDifferent(Side.BOTTOM)) {
            return false;
        }

        for (int i = 0; i < cubes.length; i++) {
            System.out.println("Cube" + i + ": " + cubes[i]);
        }
        return true;
    }

    private boolean allDifferent(Side side) {
        Set<Color> colorSet = new HashSet<>();
        for (Cube cube : cubes) {
            colorSet.add(cube.getColor(side));
        }

        return colorSet.size() == cubes.length;
    }

}
