package de.pstorch.cube;

import java.util.HashMap;
import java.util.Map;

public class Cube {

    private Orientation orientation = Orientation.first();
    private Map<Side, Color> colors = new HashMap<>();

    public Cube(Color front, Color back, Color top, Color bottom, Color left,
                Color right) {
        super();
        colors.put(Side.FRONT, front);
        colors.put(Side.BACK, back);
        colors.put(Side.TOP, top);
        colors.put(Side.BOTTOM, bottom);
        colors.put(Side.LEFT, left);
        colors.put(Side.RIGHT, right);
    }


    public Color getColor(Side facingSide) {
        return colors.get(calculateCubeSide(facingSide));
    }


    private Side calculateCubeSide(Side facingSide) {
        Side cubeSideFront = orientation.getFront();
        if (facingSide == Side.FRONT) {
            return cubeSideFront;
        }
        Side cubeSideRight = orientation.getRight();
        if (facingSide == Side.RIGHT) {
            return cubeSideRight;
        }
        if (facingSide == Side.BACK) {
            return cubeSideFront.opposite();
        }
        if (facingSide == Side.LEFT) {
            return cubeSideRight.opposite();
        }
        Side cubeSideTop = orientation.getTop();
        if (facingSide == Side.TOP) {
            return cubeSideTop;
        }
        if (facingSide == Side.BOTTOM) {
            return cubeSideTop.opposite();
        }

        return null;
    }

    public boolean setNextOrientation() {
        if (hasMoreOrientations()) {
            orientation = orientation.next();
            return true;
        }
        return false;
    }

    public void resetOrientation() {
        orientation = Orientation.first();
    }

    public boolean hasMoreOrientations() {
        return orientation.hasNext();
    }

    public Orientation getOrientation() {
        return orientation;
    }


    @Override
    public String toString() {
        return "Orientation: " + orientation.name() + ", Front: " + orientation.getFront() + "[" + getColor(Side.FRONT) + "], Top: " + orientation.getTop() + "[" + getColor(Side.TOP) + "], Back: [" + getColor(Side.BACK) + "], Bottom: [" + getColor(Side.BOTTOM) + "], Right: " + orientation.getRight();
    }

}
