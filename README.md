# Cube Solver

Tries to rotated a number of cubes, which have colored sides, in a way, so that all sides of all cubes have a different color.

## Build

Build with maven: `mvn clean install` or with a Java IDE.

## Usage

1. Create cubes
Create a number of cubes with color for each side (front, back, top, bottom, left, right):
```java
    Cube cube1 = new Cube(Color.GREEN, Color.RED, Color.BLUE, Color.YELLOW, Color.RED, Color.RED);
    Cube cubeN = ...
```

2. Add the cubes to the `CubeSolver`:
```java
    CubeSolver cubeSolver = new CubeSolver(cube1, cube2, cube3, cube4);
```

3. Run the solver:
```java
    boolean success = cubeSolver.solve();
```

## Result

It prints out the first found combination, the number of iterations needed and if it was successful or not:

```
Cube0: Orientation: BOTTOM_LEFT, Front: BOTTOM[YELLOW], Top: BACK[RED], Back: [BLUE], Bottom: [GREEN], Right: LEFT
Cube1: Orientation: BACK_BOTTOM, Front: BACK[RED], Top: LEFT[GREEN], Back: [GREEN], Bottom: [YELLOW], Right: BOTTOM
Cube2: Orientation: BACK_TOP, Front: BACK[BLUE], Top: RIGHT[YELLOW], Back: [RED], Bottom: [BLUE], Right: TOP
Cube3: Orientation: TOP_FRONT, Front: RIGHT[GREEN], Top: FRONT[BLUE], Back: [YELLOW], Bottom: [RED], Right: TOP
149743
true
```
